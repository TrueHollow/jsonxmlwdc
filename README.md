# Simple JSON/XML Web Data Connector (2019.4+)

Based on [example provided by Keshia Rose](https://repl.it/@KeshiaRose/json-xml-wdc)

## Requirements
1. Node.js version 10+

## Installation
1. Install npm dependencies (`npm install --production`)

## Starting app
1. Run standard command (`npm start`)

## Notes
App will try to get http port number from environment variable `PORT`. If variable isn't set, then app will use port 3000
