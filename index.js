const express = require('express');
const fetch = require('node-fetch');
const { parseString } = require('xml2js');

const app = express();
app.set('trust proxy', 1); // trust first proxy (used for Heroku)
const cors = require('cors');

app.use(cors());
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/views/index.html`);
});

app.post('/xml', (req, res) => {
  const { xml } = req.body;
  parseString(xml, (err, result) => {
    if (err) {
      res.status(400).json({ error: err.message });
    } else {
      res.json(result);
    }
  });
});

app.all('/proxy/*', (req, res) => {
  let url = req.url.split('/proxy/')[1];
  // fix for repl.it merging double slashes
  url = url
    .replace(/https:\/(?!\/)/, 'https://')
    .replace(/http:\/(?!\/)/, 'http://');
  const { method } = req;
  console.log(`Starting to fetch resource (${method}): ${url}`);
  fetch(url, { method })
    .then(response => {
      console.log(`Resource is loaded`);
      if (response.ok) {
        response.text().then(body => {
          console.log('Result object');
          const result = { body };
          console.log('Converting to json');
          const dataString = JSON.stringify(result);
          console.log('Sending resource to client');
          res.set('Content-Type', 'application/json');
          res.end(dataString);
          // res.send({ body });
          console.log('Resource is processed');
        });
      } else {
        res.send({ error: response.statusText });
      }
    })
    .catch(error => {
      res.send({ error: error.message });
    });
});

const PORT = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;

const listener = app.listen(PORT, () => {
  console.log(`Your app is listening on port ${listener.address().port}`);
});

module.exports = {
  app,
  listener,
};
