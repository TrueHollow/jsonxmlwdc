const fs = require('fs');
const path = require('path');
const request = require('supertest');
const { app, listener } = require('../index');

describe('basic route tests', () => {
  // eslint-disable-next-line jest/no-hooks
  beforeAll(async () => {
    // eslint-disable-next-line no-console
    console.log('Jest starting!');
  });

  beforeEach(() => {
    jest.setTimeout(15000);
  });

  afterAll(() => {
    listener.close();
    // eslint-disable-next-line no-console
    console.log('server closed!');
  });
  it('test GET index endpoint "/"', async () => {
    const response = await request(app).get('/');
    expect(response.status).toStrictEqual(200);
    const { type } = response;
    expect(type).toMatch(/text\/html/i);
  });
  it('test GET proxy endpoint "/proxy/*" (Ok)', async () => {
    const response = await request(app).get(
      '/proxy/http://localhost:3000/food.json'
    );
    expect(response.status).toStrictEqual(200);
    const { type, body: jsBody } = response;
    const { body } = jsBody;
    expect(type).toMatch(/application\/json/i);
    expect(typeof body).toBe('string');
  });
  it('test GET proxy endpoint "/proxy/*" (Not found)', async () => {
    const response = await request(app).get(
      '/proxy/http://localhost:3000/food1.json'
    );
    expect(response.status).toStrictEqual(200);
    const { type, body: jsBody } = response;
    const { body, error } = jsBody;
    expect(type).toMatch(/application\/json/i);
    expect(typeof body).toBe('undefined');
    expect(typeof error).toBe('string');
  });
  it('test GET proxy endpoint "/proxy/*" (Timeout)', async () => {
    const response = await request(app).get(
      '/proxy/http://localhost:3001/food.json'
    );
    expect(response.status).toStrictEqual(200);
    const { type, body: jsBody } = response;
    const { body, error } = jsBody;
    expect(type).toMatch(/application\/json/i);
    expect(typeof body).toBe('undefined');
    expect(typeof error).toBe('string');
  });
  it('test POST xml endpoint "/xml" (Ok)', async () => {
    const xmlString = await new Promise((resolve, reject) => {
      const xmlFilePath = path.resolve(__dirname, '..', 'public', 'orders.xml');
      fs.readFile(xmlFilePath, { encoding: 'utf8' }, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
    const response = await request(app).post('/xml').send(`xml=${xmlString}`);
    expect(response.status).toStrictEqual(200);
    const { type, body: jsBody } = response;
    expect(type).toMatch(/application\/json/i);
    expect(typeof jsBody).toBe('object');
  });
  it('test POST xml endpoint "/xml" (Invalid string)', async () => {
    const xmlString = 'fooBar';
    const response = await request(app).post('/xml').send(`xml=${xmlString}`);
    expect(response.status).toStrictEqual(400);
    const { type, body: jsBody } = response;
    expect(type).toMatch(/application\/json/i);
    expect(typeof jsBody).toBe('object');
    expect(typeof jsBody.error).toBe('string');
  });
});
