# Simple JSON/XML Web Data Connector (2019.4+)

Built by [Keshia Rose](https://twitter.com/KroseKeshia)

This is a simple [Web Data Connector](https://tableau.github.io/webdataconnector/docs/) for JSON and XML files, text, and URLs.

Simply paste in your URL or data or just drag and drop a file.

Next, choose which fields to bring in to Tableau. You can select or clear all or individual fields, or select the parent of a nested object to change the selection of all within in.

You can also add multiple tables if your data has different levels of granularity.

This WDC only allows pulling in data from one source. If you need to pull data from multiple sources it may be best to use the WDC multiple times or to create a [custom WDC](https://tableau.github.io/webdataconnector/docs/).

## Why did I build this?

I've noticed that there are a lot of people asking in our [community forums](https://community.tableau.com/) how to connect to very simple data endpoints. Mostly public data that might be refreshed every so often. The answer we usually give is to build your own WDC. While for some that may be a simple task, I felt like it shouldn't be necessary to connect to a single source of data for quick analysis. When we first launched Web Data Connectors we had a sample that allowed you to connect to JSON endpoints. We removed and stopped updating this sample once we included the ability to connect to JSON files natively in the product. But we are still missing the ability to connect to simple URLs and XML. That's why I built this WDC, hopefully, it can help people who want to analyze a simple endpoint (or file, or just some stuff they copied) to do so without a heavy lift.

## Limitations:

- Complex or super-nested data structures may fail
- Only pulls data from one URL endpoint, no pagination
- Data brought in from a file will not refresh, but URLs will
- No incremental extract refreshes

## Server Refreshes:
If you plan on using this in a production server I would suggest downloading this project and running it locally. But if you would still like to use this hosted version you will need to add it to your whitelist. You can use the following command to do so: `tsm data-access web-data-connectors add --name 'JSONXMLWDC' --url https://json-xml-wdc.keshiarose.repl.co:443 --secondary (https://code.jquery.com:443/jquery-3.4.1.min.js),(https://cdn.jsdelivr.net:443/npm/json2csv),(https://cdn.jsdelivr.net:443/npm/papaparse@5.2.0/papaparse.min.js),(https://connectors.tableau.com:443/libs/tableauwdc-2.3.latest.js)`
And then run `tsm pending-changes apply`. Note this will restart your server!